<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\homeController;
use App\Http\Controllers\ChampionsController;

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', homeController::class);
Route::get('champions', [ChampionsController::class, 'list'])->name('champions.list');
Route::get('champions/create', [ChampionsController::class, 'create'])->name('champions.create');
Route::post('champions', [ChampionsController::class, 'store'])->name('champions.store');
Route::get('champions/edit/{champion}', [ChampionsController::class, 'edit'])->name('champions.edit');
Route::put('champions/{champion}', [ChampionsController::class, 'update'])->name('champions.update');
Route::delete('champions/{champion}', [ChampionsController::class, 'destroy'])->name('champions.destroy');





