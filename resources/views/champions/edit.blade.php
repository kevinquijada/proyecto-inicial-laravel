<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

            <h1>En esta pagina podras editar</h1>
            

            <form method="post" action="{{route('champions.update', $champion)}}" class="col-md-10 offset-md-1" >   
                @csrf
                @method('put')
              <div class="form-group">
                name:<br>
                <input type="text" name="name" class="form-control form-control-lg" placeholder="nombre" value="{{$champion->name}}"><br>
                habilidades:<br>
                <input type="text" name="habilidades" class="form-control form-control-lg" placeholder="habilidades" value="{{$champion->habilidades}}"><br>
                descripcion:<br>
                <textarea name="descripcion" cols="30" rows="10" placeholder="descripcion">{{$champion->descripcion}}</textarea>


              </div>
              <div>
                  <input type="submit" value="actualizar">
                  <form action="{{route('champions.destroy', $champion)}}" method="POST">
                    @csrf
                    @method('delete')
                  <input type="submit" value="borrar">
                    </form>
              </div>
            </form>
    
</body>
</html>