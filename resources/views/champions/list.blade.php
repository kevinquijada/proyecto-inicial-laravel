<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Listado de Champions</title>

</head>
<body>
    <h1>bienvenidos a la paginas de los champions</h1>
    <div class="lista">
        <h3>lista de campeones</h3>
            <div id="lista">
                <ul>
                    @foreach ($champions as $champ)
                    <li>
                    <a href="">{{$champ->name}}</a>
                    </li>
                    <a href="{{route('champions.edit', $champ->id)}}">editar champion</a>
                    @endforeach
                </ul>
            </div>

            {{$champions->links()}}

            

            <form method="POST" action="{{route('champions.store')}}" class="col-md-10 offset-md-1" >
                @csrf
              <div class="form-group">
                name:<br>
                <input type="text" name="name" class="form-control form-control-lg" placeholder="nombre"><br>
                habilidades:<br>
                <input type="text" name="habilidades" class="form-control form-control-lg" placeholder="habilidades"><br>
                descripcion:<br>
                <textarea name="descripcion" cols="30" rows="10" placeholder="descripcion"></textarea>


              </div>

              <div>

                  <input type="submit" value="Guardar">
              </div>
            </form>
    </div>
</body>

