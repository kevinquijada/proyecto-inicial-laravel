<?php

namespace Database\Seeders;
use App\Models\Champion;
use Illuminate\Database\Seeder;

class Championseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Champion::factory(10)->create();
    }
}
