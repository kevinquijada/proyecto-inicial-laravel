<?php

namespace Database\Factories;

use App\Models\Champion;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChampionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */

    protected $model = Champion::class;
      
    public function definition()
    {
        return [
            'name' => $this->faker->sentence(),
            'habilidades' => $this->faker ->sentence(),
            'descripcion' => $this->faker ->paragraph()
        ];
    }
}
