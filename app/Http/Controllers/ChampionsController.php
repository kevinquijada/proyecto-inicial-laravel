<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Champion;
use Champions;

class ChampionsController extends Controller
{
    
    public function list(){

        $champions = champion::paginate();

        return view('champions/list', ['champions' => $champions]);

    }

    public function edit(Champion $champion){

        return view('champions/edit', ['champion' => $champion]);
    }


    public function create(){
        return view('champions/create');
    }

    public function store(Request $request){

        $champion = new Champion();

        $name = $request->input('name');

        $champion->name = $request->name;
        $champion->habilidades = $request->habilidades;
        $champion->descripcion = $request->descripcion;

        $champion->save();

        return redirect()->route('champions.list', $champion);

    }

    protected $redirectTo = '/champions.list';

    public function destroy(Champion $champion){

        $champion->delete();

        return redirect()->route('champions.list');  
    }

    public function update(Request $request, champion $champion){
        $champion->name = $request->name;
        $champion->habilidades = $request->habilidades;
        $champion->descripcion = $request->descripcion;

        $champion->save();

        return redirect()->route('champions.list', $champion);
    }
}


